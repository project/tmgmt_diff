# Translation Management Diff

Improves the reviewer experience by highlighting the differences between the previous and the current translation.

When re-translating content, especially when using machine translation, with providers like [TMGMT DeepL](https://www.drupal.org/project/tmgmt_deepl) or [TMGMT Google](https://www.drupal.org/project/tmgmt_google), it can be a daunting task for reviewers to check unchanged translations. Let's improve the reviewer experience.

### Features

Based on the previously translated content:

*   Only show changes on the Job Item edit form
*   Show a diff of the fields, close to the preview action (WIP)

#### Limitations

Supporting Paragraphs but currently not [Paragraphs Asymmetric Translation Widgets](https://www.drupal.org/project/paragraphs_asymmetric_translation_widgets).

Manually tested with Content Moderation, but unit tests still have to be written, so keeping the module in alpha until we have a good coverage.

**Structural changes**

Multivalued fields have limitations with scalars.
Here are the operations support.
Embeddable fields like Paragraphs are supporting these ones.

| Operation                   | Scalar | Embeddable field |
|-----------------------------|--------|------------------|
| Add or remove at the bottom | Yes    | Yes              |
| Add or remove elsewhere     | No     | Yes              |
| Change order                | No     | Yes              |

### Similar projects

[TMGMT Memory](https://www.drupal.org/project/tmgmt_memory). Not precisely the same use case, but can also be an interesting option for manually translated content.

### Roadmap

- ~~See how we can deal with structural changes (adding or re-ordering of paragraphs, etc.)~~
- ~~Enable features via configuration~~
- ~~Add the diff action on the Job Edit form~~
- Possibly limit to some translation providers
