<?php

namespace Drupal\tmgmt_diff\Plugin\diff\Layout;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\diff\DiffEntityComparison;
use Drupal\diff\DiffEntityParser;
use Drupal\diff\DiffLayoutBase;
use Drupal\diff\FieldReferenceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides Split fields diff layout for translations.
 *
 * Translation specific version of the 'split_fields' plugin.
 * Uses the preview entity.
 *
 * It works fine with non-embeddable fields
 * but there seem to be a bug with tmgmt preview in this context (e.g. paragraphs)
 * in some circumstances and as we are using the preview entity for the diff,
 * it doesn't cover this specific case.
 *
 * @DiffLayoutBuilder(
 *   id = "split_fields_translation_preview",
 *   label = @Translation("Split fields for translations preview"),
 *   description = @Translation("Field based layout, displays translation comparison side by side."),
 * )
 */
class SplitFieldsTranslationPreviewDiffLayout extends DiffLayoutBase {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The diff entity comparison service.
   *
   * @var \Drupal\diff\DiffEntityComparison
   */
  protected $entityComparison;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a SplitFieldsTranslationDiffLayout object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The configuration factory object.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\diff\DiffEntityParser $entity_parser
   *   The entity parser.
   * @param \Drupal\Core\DateTime\DateFormatter $date
   *   The date service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\diff\DiffEntityComparison $entity_comparison
   *   The diff entity comparison service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config, EntityTypeManagerInterface $entity_type_manager, DiffEntityParser $entity_parser, DateFormatter $date, RendererInterface $renderer, DiffEntityComparison $entity_comparison, RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $config, $entity_type_manager, $entity_parser, $date);
    $this->renderer = $renderer;
    $this->entityComparison = $entity_comparison;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('diff.entity_parser'),
      $container->get('date.formatter'),
      $container->get('renderer'),
      $container->get('diff.entity_comparison'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(ContentEntityInterface $left_revision, ContentEntityInterface $right_revision, ContentEntityInterface $entity): array {
    $build = [];
    // Build the diff comparison table.
    $diff_header = $this->buildTableHeader($left_revision, $right_revision);
    // Perform comparison only if both entity revisions loaded successfully.
    $fields = $this->compareRevisions($left_revision, $right_revision);
    // Build the diff rows for each field and append the field rows
    // to the table rows.
    $diff_rows = [];
    foreach ($fields as $field) {
      $field_label_row = '';
      if (!empty($field['#name'])) {
        $field_label_row = [
          'data' => $field['#name'],
          'colspan' => 8,
          'class' => ['field-name'],
        ];
      }

      // Process the array (split the strings into single line strings)
      // and get line counts per field.
      $this->entityComparison->processStateLine($field);

      $field_diff_rows = $this->entityComparison->getRows(
        $field['#data']['#left'],
        $field['#data']['#right']
      );

      // EXPERIMENTAL: Deal with magic thumbnail image data.
      if (isset($field['#data']['#left_thumbnail'])) {
        $field_diff_rows['#thumbnail'][1] = [
          'data' => $field['#data']['#left_thumbnail'],
          'class' => '',
        ];
      }
      if (isset($field['#data']['#right_thumbnail'])) {
        $field_diff_rows['#thumbnail'][3] = [
          'data' => $field['#data']['#right_thumbnail'],
          'class' => '',
        ];
      }

      $final_diff = [];
      $row_count_left = NULL;
      $row_count_right = NULL;

      foreach ($field_diff_rows as $key => $value) {
        $show_left = FALSE;
        $show_right = FALSE;
        if (isset($field_diff_rows[$key][1]['data'])) {
          $show_left = TRUE;
          $row_count_left++;
        }
        if (isset($field_diff_rows[$key][3]['data'])) {
          $show_right = TRUE;
          $row_count_right++;
        }
        $final_diff[] = [
          'left-line-number' => [
            'data' => $show_left ? $row_count_left : NULL,
            'class' => [
              'diff-line-number',
              isset($field_diff_rows[$key][1]['data']) ? $field_diff_rows[$key][1]['class'] : NULL,
            ],
          ],
          'left-row-sign' => [
            'data' => $field_diff_rows[$key][0]['data'] ?? NULL,
            'class' => [
              $field_diff_rows[$key][0]['class'] ?? NULL,
              isset($field_diff_rows[$key][1]['data']) ? $field_diff_rows[$key][1]['class'] : NULL,
            ],
          ],
          'left-row-data' => [
            'data' => $field_diff_rows[$key][1]['data'] ?? NULL,
            'class' => isset($field_diff_rows[$key][1]['data']) ? $field_diff_rows[$key][1]['class'] : NULL,
          ],
          'right-line-number' => [
            'data' => $show_right ? $row_count_right : NULL,
            'class' => [
              'diff-line-number',
              isset($field_diff_rows[$key][3]['data']) ? $field_diff_rows[$key][3]['class'] : NULL,
            ],
          ],
          'right-row-sign' => [
            'data' => $field_diff_rows[$key][2]['data'] ?? NULL,
            'class' => [
              $field_diff_rows[$key][2]['class'] ?? NULL,
              isset($field_diff_rows[$key][3]['data']) ? $field_diff_rows[$key][3]['class'] : NULL,
            ],
          ],
          'right-row-data' => [
            'data' => $field_diff_rows[$key][3]['data'] ?? NULL,
            'class' => isset($field_diff_rows[$key][3]['data']) ? $field_diff_rows[$key][3]['class'] : NULL,
          ],
        ];
      }

      // Add field label to the table only if there are changes to that field.
      if (!empty($final_diff) && !empty($field_label_row)) {
        $diff_rows[] = [$field_label_row];
      }

      // Add field diff rows to the table rows.
      $diff_rows = array_merge($diff_rows, $final_diff);
    }

    $build['diff'] = [
      '#type' => 'table',
      '#header' => $diff_header,
      '#rows' => $diff_rows,
      '#weight' => 10,
      '#empty' => $this->t('No visible changes'),
      '#attributes' => [
        'class' => ['diff'],
      ],
    ];

    $build['#attached']['library'][] = 'diff/diff.double_column';
    $build['#attached']['library'][] = 'diff/diff.colors';
    return $build;
  }

  /**
   * Build the header for the diff table.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $left_revision
   *   Revision from the left hand side.
   * @param \Drupal\Core\Entity\ContentEntityInterface $right_revision
   *   Revision from the right hand side.
   *
   * @return array
   *   Header for Diff table.
   */
  protected function buildTableHeader(ContentEntityInterface $left_revision, ContentEntityInterface $right_revision): array {
    $header = [];
    $header[] = [
      'data' => ['#markup' => $this->buildRevisionLink($left_revision)],
      'colspan' => 3,
    ];
    $header[] = [
      'data' => ['#markup' => $right_revision->label() . ' (' . $this->t('New translation preview') . ')'],
      'colspan' => 3,
    ];

    return $header;
  }

  /**
   * This method should return an array of items ready to be compared.
   *
   * Extracted from DiffEntityComparison::compareRevisions().
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $left_entity
   *   The left entity.
   * @param \Drupal\Core\Entity\ContentEntityInterface $right_entity
   *   The right entity.
   *
   * @return array
   *   Items ready to be compared by the Diff component.
   */
  private function compareRevisions(ContentEntityInterface $left_entity, ContentEntityInterface $right_entity): array {
    $result = [];

    $left_values = $this->parseEntity($left_entity);
    $right_values = $this->parseEntity($right_entity);

    $pluginsConfig = \Drupal::config('diff.plugins');

    foreach ($left_values as $left_key => $values) {
      [, $field_key] = explode(':', $left_key);
      // Get the compare settings for this field type.
      $compare_settings = $pluginsConfig->get('fields.' . $field_key);
      $result[$left_key] = [
        '#name' => (isset($compare_settings['settings']['show_header']) && $compare_settings['settings']['show_header'] == 0) ? '' : $values['label'],
        '#settings' => $compare_settings,
        '#data' => [],
      ];

      // Fields which exist on the right entity also.
      if (isset($right_values[$left_key])) {
        $result[$left_key]['#data'] += $this->combineFields($left_values[$left_key], $right_values[$left_key]);
        // Unset the field from the right entity so that we know if the right
        // entity has any fields that left entity doesn't have.
        unset($right_values[$left_key]);
      }
      // This field exists only on the left entity.
      else {
        $result[$left_key]['#data'] += $this->combineFields($left_values[$left_key], []);
      }
    }

    // Fields which exist only on the right entity.
    foreach ($right_values as $right_key => $values) {
      [, $field_key] = explode(':', $right_key);
      $compare_settings = $pluginsConfig->get('fields.' . $field_key);
      $result[$right_key] = [
        '#name' => (isset($compare_settings['settings']['show_header']) && $compare_settings['settings']['show_header'] == 0) ? '' : $values['label'],
        '#settings' => $compare_settings,
        '#data' => [],
      ];
      $result[$right_key]['#data'] += $this->combineFields([], $right_values[$right_key]);
    }

    return $result;
  }

  /**
   * Combine two fields into an array with keys '#left' and '#right'.
   *
   * Extracted from DiffEntityComparison::combineFields().
   *
   * @param array $left_values
   *   Entity field formatted into an array of strings.
   * @param array $right_values
   *   Entity field formatted into an array of strings.
   *
   * @return array
   *   Array resulted after combining the left and right values.
   */
  protected function combineFields(array $left_values, array $right_values): array {
    $result = [
      '#left' => [],
      '#right' => [],
    ];
    $max = max([count($left_values), count($right_values)]);
    for ($delta = 0; $delta < $max; $delta++) {
      // EXPERIMENTAL: Transform thumbnail from ImageFieldBuilder.
      // @todo Make thumbnail / rich diff data pluggable.
      // @see https://www.drupal.org/node/2840566
      if (isset($left_values[$delta])) {
        $value = $left_values[$delta];
        if (isset($value['#thumbnail'])) {
          $result['#left_thumbnail'][] = $value['#thumbnail'];
        }
        else {
          $result['#left'][] = is_array($value) ? implode("\n", $value) : $value;
        }
      }
      if (isset($right_values[$delta])) {
        $value = $right_values[$delta];
        if (isset($value['#thumbnail'])) {
          $result['#right_thumbnail'][] = $value['#thumbnail'];
        }
        else {
          $result['#right'][] = is_array($value) ? implode("\n", $value) : $value;
        }
      }
    }

    // If a field has multiple values combine them into one single string.
    $result['#left'] = implode("\n", $result['#left']);
    $result['#right'] = implode("\n", $result['#right']);

    return $result;
  }

  /**
   * Transforms an entity into an array of strings.
   *
   * Extracted from DiffEntityParser::parseEntity().
   *
   * Parses an entity's fields and for every field it builds an array of string
   * to be compared. Basically this function transforms an entity into an array
   * of strings.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   An entity containing fields.
   *
   * @return array
   *   Array of strings resulted by parsing the entity.
   */
  public function parseEntity(ContentEntityInterface $entity): array {
    $result = [];

    $diffBuilderManager = \Drupal::service('plugin.manager.diff.builder');

    // Do not load the current UI translation, as the purpose is
    // to compare translations.
    $entity_type_id = $entity->getEntityTypeId();
    // Loop through entity fields and transform every FieldItemList object
    // into an array of strings according to field type specific settings.
    /** @var \Drupal\Core\Field\FieldItemListInterface $field_items */
    foreach ($entity as $field_items) {
      // Define if the current field should be displayed as a diff change.
      $show_diff = $diffBuilderManager->showDiff($field_items->getFieldDefinition()->getFieldStorageDefinition());
      if (!$show_diff || !$entity->get($field_items->getFieldDefinition()->getName())->access('view')) {
        continue;
      }
      // Create a plugin instance for the field definition.
      $plugin = $diffBuilderManager->createInstanceForFieldDefinition($field_items->getFieldDefinition());
      if ($plugin) {
        // Create the array with the fields of the entity. Recursive if the
        // field contains entities.
        if ($plugin instanceof FieldReferenceInterface) {
          foreach ($plugin->getEntitiesToDiff($field_items) as $entity_key => $reference_entity) {
            foreach ($this->parseEntity($reference_entity) as $key => $build) {
              $result[$key] = $build;
              $result[$key]['label'] = $field_items->getFieldDefinition()->getLabel() . ' > ' . $result[$key]['label'];
            };
          }
        }
        else {
          $build = $plugin->build($field_items);
          if (!empty($build)) {
            $result[$entity->id() . ':' . $entity_type_id . '.' . $field_items->getName()] = $build;
            $result[$entity->id() . ':' . $entity_type_id . '.' . $field_items->getName()]['label'] = $field_items->getFieldDefinition()->getLabel();
          }
        }
      }
    }

    $diffBuilderManager->clearCachedDefinitions();
    return $result;
  }

  /**
   * Build the revision link for a revision.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $revision
   *   A revision where to add a link.
   *
   * @return \Drupal\Core\GeneratedLink|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   Header link for a revision in the table.
   */
  protected function buildRevisionLink(ContentEntityInterface $revision) {
    if ($revision instanceof RevisionLogInterface) {
      $revision_date = $this->date->format($revision->getRevisionCreationTime(), 'short');
      $revision_link = Link::fromTextAndUrl($revision->label(), $revision->toUrl('revision'))->toString() . ' (' . $revision_date . ')';
    }
    else {
      $revision_link = Link::fromTextAndUrl($revision->label(), $revision->toUrl('revision'))
        ->toString();
    }
    return $revision_link;
  }

}
