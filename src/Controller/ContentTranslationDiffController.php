<?php

namespace Drupal\tmgmt_diff\Controller;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt_content\Controller\ContentTranslationPreviewController;
use Drupal\tmgmt_content\Plugin\tmgmt\Source\ContentEntitySource;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Content diff translation controller.
 */
class ContentTranslationDiffController extends ContentTranslationPreviewController {

  /**
   * Preview job item entity data as a diff.
   *
   * @param \Drupal\tmgmt\JobItemInterface $tmgmt_job_item
   *   Job item to be previewed.
   * @param string $view_mode
   *   The view mode that should be used to display the entity.
   *
   * @return array
   *   A render array.
   */
  public function diffPreview(JobItemInterface $tmgmt_job_item, $view_mode): array {
    // Load entity.
    $entity = ContentEntitySource::load($tmgmt_job_item->getItemType(), $tmgmt_job_item->getItemId(), $tmgmt_job_item->getJob()->getSourceLangcode());

    // We cannot show the preview for non-existing entities.
    if (!$entity) {
      throw new NotFoundHttpException();
    }

    // If the tempstore is empty, there is nothing to compare.
    $store = \Drupal::service('tempstore.private')->get('tmgmt_diff');
    $diffData = $store->get('job_item_' . $tmgmt_job_item->id());
    if (empty($diffData)) {
      throw new NotFoundHttpException();
    }

    /** @var \Drupal\tmgmt_diff\JobItemDiff $jobItemDiff */
    $jobItemDiff = \Drupal::service('tmgmt_diff.job_item');
    $translatedEntity = $jobItemDiff->getTranslatedEntity($tmgmt_job_item);

    $data = $tmgmt_job_item->getData();
    $targetLangcode = $tmgmt_job_item->getJob()->getTargetLangcode();
    // Populate preview with target translation data.
    $tmgmtPreviewEntity = $this->makePreview($entity, $data, $targetLangcode);
    // Set the entity into preview mode.
    $tmgmtPreviewEntity->in_preview = TRUE;

    $build = $this->compareTranslationsFromTempStore($translatedEntity, $tmgmtPreviewEntity, $entity, $tmgmt_job_item);
    // Needs preview to work as expected with paragraphs and not rely on render context.
    // $build = $this->compareTranslationsFromPreview($translatedEntity, $tmgmtPreviewEntity, $entity, $view_mode);
    // The preview is not cacheable.
    $build['#cache']['max-age'] = 0;
    \Drupal::service('page_cache_kill_switch')->trigger();

    return $build;
  }

  /**
   * The _title_callback for the page that renders a single node in preview.
   *
   * @param \Drupal\tmgmt\JobItemInterface $tmgmt_job_item
   *   The current node.
   *
   * @return string
   *   The page title.
   */
  public function title(JobItemInterface $tmgmt_job_item): string {
    $target_language = $tmgmt_job_item->getJob()->getTargetLanguage()->getName();
    $entity = ContentEntitySource::load($tmgmt_job_item->getItemType(), $tmgmt_job_item->getItemId(), $tmgmt_job_item->getJob()->getSourceLangcode());
    $title = $entity->label();

    return $this->t("Diff of @title for @target_language", [
      '@title' => $title,
      '@target_language' => $target_language,
    ]);
  }

  /**
   * Compares latest saved translation with the diff from the form review.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $translated_entity
   * @param \Drupal\Core\Entity\ContentEntityInterface $tmgmt_preview_entity
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   * @param \Drupal\tmgmt\JobItemInterface $tmgmt_job_item
   *
   * @return array|array[]
   */
  public function compareTranslationsFromTempStore(
    ContentEntityInterface $translated_entity,
    ContentEntityInterface $tmgmt_preview_entity,
    ContentEntityInterface $entity,
    JobItemInterface $tmgmt_job_item
  ): array {
    $build = [
      'header' => [
        '#prefix' => '<header class="diff-header">',
        '#suffix' => '</header>',
      ],
      'controls' => [
        '#prefix' => '<div class="diff-controls">',
        '#suffix' => '</div>',
      ],
    ];

    $filter = 'split_fields_translation';
    $diffLayoutManager = \Drupal::service('plugin.manager.diff.layout');
    // Perform comparison only if both entity revisions loaded successfully.
    if ($tmgmt_preview_entity != FALSE && $translated_entity != FALSE) {
      // Build the diff comparison with the plugin.
      if ($plugin = $diffLayoutManager->createInstance($filter)) {
        $build = array_merge_recursive($build, $plugin->build($translated_entity, $tmgmt_preview_entity, $entity, $tmgmt_job_item));
        $build['diff']['#prefix'] = '<div class="diff-responsive-table-wrapper">';
        $build['diff']['#suffix'] = '</div>';
        $build['diff']['#attributes']['class'][] = 'diff-responsive-table';
      }
    }

    $build['#attached']['library'][] = 'diff/diff.general';
    return $build;
  }

  /**
   * Compares latest saved translation with the tmgmt preview entity.
   *
   * Uses the split_fields_translation_preview plugin to compare two translations.
   * There is a bug with the preview of paragraphs in some circumstances,
   * so we are not using it for now.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $translated_entity
   * @param \Drupal\Core\Entity\ContentEntityInterface $tmgmt_preview_entity
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   * @param string $view_mode
   *
   * @return array
   */
  public function compareTranslationsFromPreview(
    ContentEntityInterface $translated_entity,
    ContentEntityInterface $tmgmt_preview_entity,
    ContentEntityInterface $entity,
    string $view_mode
  ): array {
    $build = [
      'header' => [
        '#prefix' => '<header class="diff-header">',
        '#suffix' => '</header>',
      ],
      'controls' => [
        '#prefix' => '<div class="diff-controls">',
        '#suffix' => '</div>',
      ],
    ];

    // The DiffEntityParser is loading the entity translation
    // based on the current language. We need to rewrite some parts
    // to be able to make use of it for comparing two translations.
    // So we are using a translation specific split_fields plugin
    // implementation.
    $filter = 'split_fields_translation_preview';
    $diffLayoutManager = \Drupal::service('plugin.manager.diff.layout');
    // Perform comparison only if both entity revisions loaded successfully.
    if ($tmgmt_preview_entity != FALSE && $translated_entity != FALSE) {
      // Build the diff comparison with the plugin.
      if ($plugin = $diffLayoutManager->createInstance($filter)) {
        $build = array_merge_recursive($build, $plugin->build($translated_entity, $tmgmt_preview_entity, $entity));
        $build['diff']['#prefix'] = '<div class="diff-responsive-table-wrapper">';
        $build['diff']['#suffix'] = '</div>';
        $build['diff']['#attributes']['class'][] = 'diff-responsive-table';
      }
    }

    $build['#attached']['library'][] = 'diff/diff.general';
    return $build;
  }

}
