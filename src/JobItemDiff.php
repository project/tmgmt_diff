<?php

namespace Drupal\tmgmt_diff;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Entity\ThirdPartySettingsInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Plugin\DataType\EntityReference;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Entity\TranslatableRevisionableStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\tmgmt\Data;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt\SourcePreviewInterface;
use Drupal\tmgmt_content\Plugin\tmgmt\Source\ContentEntitySource;

/**
 * A service for Job Item Diff.
 */
class JobItemDiff {

  use StringTranslationTrait;

  /**
   * Translation Management data service.
   *
   * @var \Drupal\tmgmt\Data
   */
  protected $tmgmtData;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Content entity source Translation Management plugin instance.
   *
   * @var \Drupal\tmgmt_content\Plugin\tmgmt\Source\ContentEntitySource
   */
  private $contentEntitySource;

  /**
   * Constructs a new JobItemDiff.
   *
   * @param \Drupal\tmgmt\Data $tgmt_data
   *   Translation Management data service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   */
  public function __construct(
    Data $tgmt_data,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory
  ) {
    $this->tmgmtData = $tgmt_data;
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    // @todo don't use this pattern.
    $this->contentEntitySource = \Drupal::service('plugin.manager.tmgmt.source')->createInstance('content');
  }

  /**
   * Hide unchanged translations from the Job Item edit form.
   *
   * Compares latest revision of the Node
   * (and not the latest Job, as the Node can contain changes).
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function hideUnchanged(array &$form, FormStateInterface $form_state): void {
    $jobItem = $form_state->getFormObject()->getEntity();
    $translatedEntity = $this->getTranslatedEntity($jobItem);

    if (!$translatedEntity instanceof ContentEntityInterface) {
      return;
    }

    // Get the translatable data from the latest revision of the entity,
    // so we can compare the current translation from the previous one
    // and hide the fields that have not changed.
    // When using paragraphs, it does not load the right language
    // so replace ContentEntitySource::extractTranslatableData($translatedEntity)
    // with $this->extractTranslatableData($translatedEntity)
    $translatableData = $this->extractTranslatableData($translatedEntity);

    $hasChanges = FALSE;
    // Gather all diff data to be used in the diff controller.
    $diffData = [];

    foreach ($form['review'] as $fieldKey => $formElement) {
      if (!str_starts_with($fieldKey, '#')) {
        if (!empty($translatableData[$fieldKey])) {
          $flattenedTranslatableData = $this->tmgmtData->flatten($translatableData[$fieldKey], $fieldKey);
          // Extract all keys from the formElement.
          $formElementValueKeys = [];
          $unchangedItems = 0;

          foreach (Element::children($formElement) as $formElementKey) {
            $formElementValueKeys[] = $formElementKey;
            $dataKey = str_replace('|', '][', $formElementKey);
            $dataKey = $dataKey . '][value';
            if (
              !empty($flattenedTranslatableData[$dataKey]['#text']) &&
              !empty($formElement[$formElementKey][$formElementKey . '|value']['translation']['#default_value'])
            ) {
              $fieldValue = $flattenedTranslatableData[$dataKey]['#text'];
              $translatedFieldValue = $this->processTranslatedContent($fieldValue);
              $formElementValue = $formElement[$formElementKey][$formElementKey . '|value']['translation']['#default_value'];
              if ($formElementValue === $translatedFieldValue) {
                $groupLabel = '';
                if (!empty($form['review'][$fieldKey][$formElementKey]['#group_label'][0])) {
                  $groupLabel = $form['review'][$fieldKey][$formElementKey]['#group_label'][0] . ' - ';
                }
                $groupLabel .= $this->t('Translation unchanged');
                $form['review'][$fieldKey][$formElementKey]['#group_label'][0] = $groupLabel;
                $form['review'][$fieldKey][$formElementKey]['#access'] = FALSE;
                $unchangedItems++;
              }
              else {
                // Massage data so it complies with what the DiffLayout plugin
                // is expecting.
                $labelParts = [$formElement['#top_label']];
                if (
                  !empty($form['review'][$fieldKey][$formElementKey]['#group_label'] &&
                  is_array($form['review'][$fieldKey][$formElementKey]['#group_label']))
                ) {
                  foreach ($form['review'][$fieldKey][$formElementKey]['#group_label'] as $groupLabel) {
                    $labelParts[] = $groupLabel;
                  }
                }

                $diffData[$fieldKey . ':' . $formElementKey] = [
                  '#name' => implode(' > ', $labelParts),
                  '#settings' => NULL,
                  '#data' => [
                    '#left' => $translatedFieldValue,
                    '#right' => $formElementValue,
                  ],
                ];
              }
            }
          }

          // If all items are unchanged, hide the whole field.
          if ($unchangedItems === count($formElementValueKeys)) {
            $form['review'][$fieldKey]['#access'] = FALSE;
          }
          else {
            $hasChanges = TRUE;
          }
        }
      }
    }

    if (!$hasChanges) {
      $form['review']['#markup'] = t('✅ There are no changes to review between the previous translation and this one. You can save the job as completed.');
    }
    else {
      // @todo when job is saved as completed, delete the diff data.
      $store = \Drupal::service('tempstore.private')->get('tmgmt_diff');
      $store->set('job_item_' . $jobItem->id(), $diffData);
    }
  }

  /**
   * Returns the translated entity if it exists.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getTranslatedEntity(JobItemInterface $tmgmt_job_item): ContentEntityInterface | NULL {
    $entityId = $tmgmt_job_item->getItemId();
    $entityTypeId = $tmgmt_job_item->getItemType();
    $translationLangcode = $tmgmt_job_item->getJob()->getTargetLangcode();

    $storage = $this->entityTypeManager->getStorage($entityTypeId);
    $entity = $storage->load($entityId);

    // If the entity is not translatable, we shouldn't reach that point though.
    // As this is a service, just keep this check to make sure that it
    // implements the interface.
    if (!$entity instanceof TranslatableInterface) {
      return NULL;
    }

    // If the entity is revisionable, get the latest translation affected revision.
    if ($storage->getEntityType()->isRevisionable() && $storage instanceof TranslatableRevisionableStorageInterface) {
      $revisionId = $storage->getLatestTranslationAffectedRevisionId($entity->id(), $translationLangcode);
      // If there is no revision yet, there is nothing to compare.
      if (!$revisionId) {
        return NULL;
      }
      $entity = $storage->loadRevision($revisionId);
    }

    // If this is the first translation, there is nothing to compare.
    if (!$entity->hasTranslation($translationLangcode)) {
      return NULL;
    }

    // Get the latest translated revision of the entity.
    // Offload to content entity source as it already handles revisionable content.
    // @todo check why it does not get the correct translation.
    //   See if (ContentEntitySource::isModeratedEntity($translation))
    // $translatedEntity = ContentEntitySource::load($entityTypeId, $entityId, $translationLangcode);
    return $entity->getTranslation($translationLangcode);
  }

  /**
   * There can be some differences with html entities between the
   * form values and what we get back from the translation provider.
   *
   * @todo test this with multiple providers.
   */
  private function processTranslatedContent(string $content): string {
    $processedContent = html_entity_decode($content);
    return $processedContent;
  }

  /**
   * Extracts translatable data from an entity.
   *
   * Port of ContentEntitySource::extractTranslatableData().
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to get the translatable data from.
   *
   * @return array
   *   Translatable data.
   */
  public function extractTranslatableData(ContentEntityInterface $entity): array {
    $field_definitions = $entity->getFieldDefinitions();
    $exclude_field_types = ['language'];
    $exclude_field_names = ['moderation_state'];

    /** @var \Drupal\content_translation\ContentTranslationManagerInterface $content_translation_manager */
    $content_translation_manager = \Drupal::service('content_translation.manager');
    $is_bundle_translatable = $content_translation_manager->isEnabled($entity->getEntityTypeId(), $entity->bundle());

    // Exclude field types from translation.
    $translatable_fields = array_filter($field_definitions, function (FieldDefinitionInterface $field_definition) use ($exclude_field_types, $exclude_field_names, $is_bundle_translatable) {

      if ($is_bundle_translatable) {
        // Field is not translatable.
        if (!$field_definition->isTranslatable()) {
          return FALSE;
        }
      }
      elseif (!$field_definition->getFieldStorageDefinition()->isTranslatable()) {
        return FALSE;
      }

      // Field type matches field types to exclude.
      if (in_array($field_definition->getType(), $exclude_field_types)) {
        return FALSE;
      }

      // Field name matches field names to exclude.
      if (in_array($field_definition->getName(), $exclude_field_names)) {
        return FALSE;
      }

      // User marked the field to be excluded.
      if ($field_definition instanceof ThirdPartySettingsInterface) {
        $is_excluded = $field_definition->getThirdPartySetting('tmgmt_content', 'excluded', FALSE);
        if ($is_excluded) {
          return FALSE;
        }
      }
      return TRUE;
    });

    \Drupal::moduleHandler()->alter('tmgmt_translatable_fields', $entity, $translatable_fields);

    $data = [];
    foreach ($translatable_fields as $field_name => $field_definition) {
      $field = $entity->get($field_name);
      // ---------------- START custom ----------------
      // Accessing protected method getFieldProcessor.
      $reflectionMethod = new \ReflectionMethod(get_class($this->contentEntitySource), 'getFieldProcessor');
      $reflectionMethod->setAccessible(TRUE);
      $data[$field_name] = $reflectionMethod->invoke($this->contentEntitySource, $field_definition->getType())->extractTranslatableData($field);
      // $data[$field_name] = $this->contentEntitySource->getFieldProcessor($field_definition->getType())->extractTranslatableData($field);
      // ---------------- END custom ----------------
    }

    $embeddable_fields = ContentEntitySource::getEmbeddableFields($entity);
    foreach ($embeddable_fields as $field_name => $field_definition) {
      $field = $entity->get($field_name);

      /** @var \Drupal\Core\Field\FieldItemInterface $field_item */
      foreach ($field as $delta => $field_item) {
        foreach ($field_item->getProperties(TRUE) as $property_key => $property) {
          // If the property is a content entity reference, and it's value is
          // defined, then we call this method again to get all the data.
          if ($property instanceof EntityReference && $property->getValue() instanceof ContentEntityInterface) {
            // All the labels are here, to make sure we don't have empty
            // labels in the UI because of no data.
            $data[$field_name]['#label'] = $field_definition->getLabel();
            if (count($field) > 1) {
              // More than one item, add a label for the delta.
              $data[$field_name][$delta]['#label'] = t('Delta #@delta', ['@delta' => $delta]);
            }
            // Get the referenced entity.
            $referenced_entity = $property->getValue();
            // Get the source language code.
            $langcode = $entity->language()->getId();
            // If the referenced entity is translatable and has a translation
            // use it instead of the default entity translation.
            // ---------------- START custom ----------------
            // As paragraphs are not translatable by design, just remove this check
            // $content_translation_manager->isEnabled($referenced_entity->getEntityTypeId(), $referenced_entity->bundle())
            // if ($content_translation_manager->isEnabled($referenced_entity->getEntityTypeId(), $referenced_entity->bundle()) && $referenced_entity->hasTranslation($langcode)) {.
            if ($referenced_entity->hasTranslation($langcode)) {
              $referenced_entity = $referenced_entity->getTranslation($langcode);
            }
            // ---------------- END custom ----------------
            $data[$field_name][$delta][$property_key] = $this->extractTranslatableData($referenced_entity);
            // Use the ID of the entity to identify it later, do not rely on the
            // UUID as content entities are not required to have one.
            $data[$field_name][$delta][$property_key]['#id'] = $property->getValue()->id();
          }
        }

      }
    }
    return $data;
  }

  /**
   * Compare the current job preview with the latest revision of the entity.
   *
   * @param array $form
   *
   * @return void
   */
  public function addDiffAction(array &$form, FormStateInterface $form_state): void {
    $jobItem = $form_state->getFormObject()->getEntity();
    $sourcePlugin = $jobItem->getSourcePlugin();
    $translatedEntity = $this->getTranslatedEntity($jobItem);
    // @todo display the diff only if there is something to compare.
    if (
      // Display the diff action only if there was a previous translation.
      $translatedEntity instanceof ContentEntityInterface &&
      // If it has a preview url, assume that we can use a diff one.
      $sourcePlugin instanceof SourcePreviewInterface &&
      $this->getDiffUrl($jobItem)
    ) {
      $form['actions']['diff'] = [
        '#type' => 'link',
        '#title' => t('Diff'),
        '#url' => $this->getDiffUrl($jobItem),
        '#attributes' => [
          'target' => '_blank',
          'class' => ['action-link'],
        ],
        // Right after the "Preview" button.
        '#weight' => 25,
      ];
    }
  }

  /**
   * Returns the diff url for a job item.
   *
   * @param \Drupal\tmgmt\JobItemInterface $job_item
   *
   * @return \Drupal\Core\Url|null
   */
  public function getDiffUrl(JobItemInterface $job_item): Url | NULL {
    if (
      $job_item->getJob()->isActive() &&
      !($job_item->isAborted() || $job_item->isAccepted())
    ) {
      return new Url(
        'tmgmt_diff.job_item_diff',
        ['tmgmt_job_item' => $job_item->id()],
        ['query' => ['key' => \Drupal::service('tmgmt_content.key_access')->getKey($job_item)]]
      );
    }
    else {
      return NULL;
    }
  }

}
