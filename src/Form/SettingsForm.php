<?php

namespace Drupal\tmgmt_diff\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Config form for the tmgmt_diff module.
 *
 * @package Drupal\tmgmt_diff\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return [
      'tmgmt_diff.settings',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'tmgmt_diff_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('tmgmt_diff.settings');
    $form['enabled_features'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled features'),
      '#description' => $this->t('Select which features should be enabled on the Job Edit form.'),
      '#default_value' => $config->get('enabled_features') ?? [],
      '#options' => [
        'hide_unchanged' => $this->t('Hide unchanged items'),
        'diff_action' => $this->t('Show diff action'),
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('tmgmt_diff.settings')
      ->set('enabled_features', $form_state->getValue('enabled_features'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
